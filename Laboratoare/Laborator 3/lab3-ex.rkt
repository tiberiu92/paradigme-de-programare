#lang racket

;; O functie foarte utila este build-list. Aceasta primeste un numar n si o functie unara f si intoarce
;; lista formata prin aplicarea functiei f asupra fiecarui numar de la 0 la n - 1
;; Exemple:
;; (build-list 10 (λ(x) x)) ; -> '(0 1 2 3 4 5 6 7 8 9)
;; (build-list 10 (λ(x) (* x x))) ; -> (0 1 4 9 16 25 36 49 64 81)
;; (build-list 5 (λ(x) 0)) ; -> '(0 0 0 0 0)

;; Functii curry si uncurry
;;--------------------------

;; 1. (0.5p)
;; sa se implementeze functia uncurry->curry, dupa urmatoarea specificatie:
;; fiind data o functie uncurry f care ia in Scheme 2 argumente,
;; (uncurry->curry f) trebuie sa produca o functie curry cu acelasi efect ca functia f
;; practic uncurry->curry este o functie care primeste mai intai ca parametru o functie
;; uncurry, apoi argumentele acesteia (pe rand, pt ca altfel (uncurry->curry f) nu ar 
;; fi o functie curry) si face exact ce facea f asupra acelor argumente
;; (((uncurry->curry >) 5) 4) => #t
;(define uncurry->curry
;  (lambda (f)
;    (lambda (x)
;      (lambda (y)

(display "1. ")

(define uncurry->curry 
  (lambda (f)
    (lambda (x)
      (lambda (y)
        (f x y)
      )
    )
  )
)

(((uncurry->curry >) 5) 4)

;; 2. (0.5p)
;; sa se implementeze functia curry->uncurry (procesul invers celui de la punctul 1)
;; acum (curry->uncurry g) va produce o functie uncurry dintr-o functie curry g de 
;; 2 argumente
; (define curry->uncurry

(display "2. ")

(define curry->uncurry
  (lambda (g)
    (lambda (x y)
      ((g x) y)
    )
  )
)

((curry->uncurry (uncurry->curry >)) 5 4)

;; Functionale
;;------------

;; functionala = o functie care primeste functii ca parametri sau returneaza functii
;; in specificatiile de mai jos
;; f = functie binara
;; p = predicat (functie care testeaza o proprietate a argumentului
;; sau si intoarce true sau false)
;; L, L1, L2 = liste
;; x = element in lista


;; 3. (1p)
;; sa se implementeze functionala foldright
;; (foldright f init lst) acumuleaza aplicatia functiei binare f pe lista lst in felul urmator:
;; mai intai (f last init) => rezultat1 [last este ultimul element din lst]
;; apoi (f last1 rezultat1) => rezultat2, (f last2 rezultat2) => rezultat3 etc
;; [last1 e penultimul element, last2 cel dinaintea-i]
;; pana la (f (car lst) rezultat-pana-acum)
;(define foldright
;  (λ (f init lst)

(display "3. ")

(define foldright
  (λ (f init L)
    (if(null? L)
       init
       (f (car L) (foldright f init (cdr L)))
    )
  )
)

(foldright + 0 '(1 2 3 4))


;;4. (1p)
;;Sa se implementeze functionala map folosind foldright
;;(my-map f L) aplica f asupra fiecarui element din L si returneaza lista rezultatelor
;(define my-map
;  (λ (f L)

;;(my-map (λ (e) (* 2 e)) '(1 2 3 4 5)) ;; => '(2 4 6 8 10)

(display "4. ")

(define my-map
  (λ (f L)
    (foldright (λ (x y) (cons (f x) y)) '() L)
  )
)

(my-map (λ (e) (* 2 e)) '(1 2 3 4 5))


;;5. (1p)
;;Sa se implementeze functionala filter folosind foldright
;;(my-filter p L) returneaza lista elementelor din L pentru care predicatul p returneaza #t
;(define my-filter
;  (λ (p L)

;;(my-filter (λ (l) (not (null? l))) '((1 2 3) () (7 6 5) () () (9) ())) ;;=> '((1 2 3) (7 6 5) (9))

(display "5. ")

(define my-filter
  (λ (p L)
    (foldright 
     (λ (x y) (if (p x) (cons x y) y)) '() L)))


(my-filter (λ (l) (not (null? l))) '((1 2 3) () (7 6 5) () () (9) ())) ;;=> '((1 2 3) (7 6 5) (9))

;;Matrice
;;-------

;; Utilizati functionale pentru a implementa cat mai pe scurt functiile urmatoare.
;; matrice = lista de liste. Numar de linii = (length m). Numar de coloane = (length (car l))
;;
;; Pentru o parte din exercitiile urmatoare ar putea fi de ajutor urmatoarele functii:
;; I_n, care primeste un numar n si intoarce matricea unitate n x n
;; Observati implementarea scurta utilizand functionale.
(define (I_n n)
  (map (λ(k) (build-list n (λ(x) (if (= x k) 1 0)))) (build-list n (λ(x) x))))
;; (I_n 3) ; -> ((1 0 0) (0 1 0) (0 0 1))

;; O_mn, care primeste doua numere m si n si intoarce o matrice plina de 0 de dimensiune m x n
(define (O_mn m n)
  (build-list m (λ(l) (build-list n (λ(e) 0)))))
;; (O_mn 3 4) ; -> ((0 0 0 0) (0 0 0 0) (0 0 0 0))

;; num-mul-matrix primeste o valoare k si o matrice M si inmulteste valoarea cu matricea M
(define (num-mul-matrix k M)
  (map (λ(L) (map (λ(e) (* e k)) L)) M))
;; (num-mul-matrix 10 '((1 2 3) (4 5 6))) ;-> '((10 20 30) (40 50 60))


;;6. (0.5p)
;;Sa se implementeze functia get-line
;;(get-line l M) returneaza a l-a linie a matricei M. Liniile sunt indexate de la 1
;;Hint: linia l este al l-lea element din lista.
;(define get-line
;  (λ (l M)

(display "6. ")

(define get-line
  (λ (l M)
    (if(= 1 l) (car M)
       (get-line (- l 1) (cdr M))
    )
  )
)

(get-line 2 '((1 2 3 4) (5 6 7 8)))

    
;;(get-line 2 '((1 2 3 4) (5 6 7 8))) ;;=> '(5 6 7 8)


;;7. (0.5p)
;;Sa se implementeze functia get-column
;;(get-column c M) returneaza a c-a coloana a matricei M. Coloanele sunt indexate de la 1.
;;Hint: coloana c contine al c-lea element din fiecare linie
;(define get-column
;  (λ (c M)

;;(get-column 2 '((1 2) (3 4) (5 6) (7 8))) ;;=> '(2 4 6 8)

(display "7. ")

(define get
  (λ (n L)
    (if (= n 1) (car L)
        (get (- n 1) (cdr L))
    )
  )
)
    
(define get-column
  (λ (c M)
    (if (= (length M) 0) '()
        (cons (get c (car M)) (get-column c (cdr M)))
    )
  )
)

(get-column 2 '((1 2) (3 4) (5 6) (7 8)))

;;8. (2p)
;;Sa se implementeze functia transpose folosind cel putin o functionala
;;(transpose M) returneaza transpusa matricei M
;(define transpose
;  (λ (M)

;;(transpose '((1 2 3) (4 5 6))) ;;=> '((1 4) (2 5) (3 6)) 

(display "8. ")

(define transpose
  (λ (M)
    (cond
      ((null? M) '())
      ((null? (car M)) '())
      (else (cons (map car M) (transpose (map cdr M))))
    )
  )
)

(transpose '((1 2 3) (4 5 6)))

;;9. (1p)
;;Sa se implementeze functia add-matrix folosind cel putin o functionala
;;(add-matrix M1 M2) returneaza suma matricelor M1 si M2.
;(define add-matrix
;  (λ (M1 M2)

;;(add-matrix '((1 2 3 4) (6 7 8 9)) '((1 2 2 1) (0 1 3 5))) ;;=> '((2 4 5 5) (6 8 11 14))

(display "9. ")

(define add-matrix
  (λ (M1 M2)
    (map (λ (x y) (map + x y)) M1 M2)
  )
)

(add-matrix '((1 2 3 4) (6 7 8 9)) '((1 2 2 1) (0 1 3 5)))


;;10. (1p)
;;Sa se implementeze functia mul-matrix folosind cel putin o functionala
;;(mul-matrix M1 M2) returneaza produsul matricelor M1 si M2.
;(define mul-matrix
;  (λ (M1 M2)>
    
;;(mul-matrix '((1 2) (3 4) (5 6)) '((7 8 9) (10 11 12))) ;;=> '((27 30 33) (61 68 75) (95 106 117))

(display "10. ")

(define mul-matrix
  (λ (M1 M2)
    (map (λ (x) (apply map (λ y (apply + (map * x y)))M2)) M1)
  )
) 


(mul-matrix '((1 2) (3 4) (5 6)) '((7 8 9) (10 11 12)))     

;;11. (1p)
;;Sa se implementeze functia pow-matrix folosind cel putin o functionala
;;(pow-matrix M p) returneaza rezultatul ridicarii matricei M la puterea p. p este natural.
;(define pow-matrix
;  (λ (M p)
    
;;(pow-matrix '((1 2) (3 4)) 2) ;;=> '((7 10) (15 22))

(display "11. ")

(define pow-matrix
  (λ (M p)
    (if (= p 1) M
        (mul-matrix M (pow-matrix M (- p 1)))
    )
  )
)

(pow-matrix '((1 2) (3 4)) 2)
        
;;12. Bonus (2p)
;;Sa se implementeze functia exp-matrix
;;((exp-matrix n) M) returneaza aproximarea e^M (unde M este o matrice) folosind primii n termeni din seria Taylor.
;;Utilizati urmatoarea formula: e^M = (1/0!) * M^0 + (1/1!) * M^1 + (1/2!) * M^2 + ... (1/(n-1)!) * M^(n - 1)
;(define exp-matrix
;  (λ (n)
;    (λ (M)

;;((exp-matrix 20) '((1 2) (3 4))) ;;=> '((51.96 74.73) (112.10 164.07))