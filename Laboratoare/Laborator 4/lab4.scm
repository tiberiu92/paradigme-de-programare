; 1. (2p)
; Sa se scrie o functie care implementeaza quicksort. Folositi o
; constructie de tip let pentru a scrie cat mai putin cod.
;(define qsort
;  (lambda (numbers)

; puteti testa folosind:
;(qsort '(5 4 3 2 1))


; 2. (4p)
; Sa se scrie o functie care gaseste radacina unei functii date.
; Mai multe informatii la http://en.wikipedia.org/wiki/Bisection_method

; Se dau urmatoarele:
; o functie, f1 -- f1(x) = x + 5
; un numar maxim de iteratii (MAX_ITER)
; o valoare a preciziei cerute (EPS)
(define f1
  (lambda (x)
    (+ x 5)))

(define MAX_ITER 100)
(define EPS 0.001)

; Veti folosi un named let pentru a simula iteratia.
;(define bisection
;  (lambda (f)
;    (let bisect (...)

; testati folosind urmatoarele doua apeluri:
;(bisection f1)
;(exact->inexact (bisection f1))
; (exact->inexact se foloseste pentru a converti numere rationale in float
; puteti schimba valorile MAX_ITER si EPS pentru a experimenta


; 3. (4p)
; Sa se scrie o functie care verifica daca un sir de caractere este palindrom.
; ATENTIE! Sirul va fi dat ca parametru sub forma de lista de caractere.
; Aceasta se obtine folosind functia string->list.
; ex: (string->list "test") => (#\t #\e #\s #\t)
; hint: comparatorii "<", ">" etc functioneaza si pe caractere.
; Folositi o constructie let (let, let* etc.) pentru a scrie cat mai putin cod.

;(define isPal?
;  (lambda (L)
;    (let* (...)



; testati folosind:
;(isPal? (string->list "abc")) ; #f
;(isPal? (string->list "aba")) ; #t
;(isPal? (string->list "abcd")); #f
;(isPal? (string->list "abba")); #t


; 4. (2p)
; Fie urmatoarea codificare pentru nodul unui arbore binar de cautare:
; (valoare subarb-stanga subarb-dreapta)
; arbore vid == ()
; Scrieti o functie care efectueaza o parcurgere in inordine a unui
; arbore dat si genereaza lista nodurilor (in inordine).
; Folositi let.
;(define inorder
;  (lambda (T)

; testati folosind:
;(inorder '(4 (3 () ()) (17 (5 () ()) ()))) ; => (3 4 5 17)