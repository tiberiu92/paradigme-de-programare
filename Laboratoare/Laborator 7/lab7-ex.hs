{-# LANGUAGE NoMonomorphismRestriction #-}
import Data.List
import Data.Maybe
import Debug.Trace
import Test.QuickCheck

{-
Expresia `undefined` are orice tip dar nu poate fi evaluată.
-}

data TList a = Cons a (TList a) | Nil deriving Show

{-
 - tlistToList transforma o TList intr-o lista Haskell
 -}
tlistToList :: TList a -> [a]
tlistToList Nil = []
tlistToList (Cons x y) = x : tlistToList y

{-
 - listToTList transforma o TList intr-o lista Haskell
 -}
listToTList :: [a] -> TList a
listToTList [] = Nil
listToTList (x:xs) = Cons x (listToTList xs)

{-
1. (2p)
Pe baza tipului de date TList din documentatie, scrieti functiile
pentru concatenarea a 2 TLists si lungimea unei TList
-}

concatTList :: TList a -> TList a -> TList a
concatTList Nil list2 = list2
concatTList (Cons a l) l2 = Cons a (concatTList l l2)

lengthTList :: TList a -> Int 
lengthTList Nil = 0
lengthTList (Cons a l) = 1 + (lengthTList l)

checkConcat = concatTList (Cons 2 (Cons 3 (Cons 1 Nil))) (Cons 5 (Cons 8 Nil))
-- (Cons 2 (Cons 3 (Cons 1 (Cons 5 (Cons 8 Nil)))))

checkLength = lengthTList (Cons 2 (Cons 4 (Cons 8 Nil))) 
-- 3

{-
 2. (3p)
Definiti un tip de date SList a care sa aiba functionalitati asemanatoare
listelor din Scheme, permitand componente la diferite nivele de imbricare.
Ex: Lista din Scheme '(1 (3 4) (2)) sa poata sa fie fi definita in Haskell
folosind SList.
Aditional, definiti:
- sTake, intoarce elementul de pe pozitia n
- flatten, o functie ce reduce gradul de indentare al SList-ului primit
- deepLength, o functie ce efectiv numarul de elemente de tip a din SList
Notare:
(1p) definirea tipului de date si sTake
(1p) flatten
(1p) deepLength
 -}

data SList a = Nils | ConsSt (SList a) (SList a) | ConsS a (SList a) deriving Show

concatSList :: SList a -> SList a -> SList a
concatSList Nils list2 = list2
concatSList (ConsS a l) l2 = ConsS a (concatSList l l2)

sTake :: Int -> SList a -> SList a
sTake n Nils = Nils
sTake 1 (ConsS a l) = ConsS a Nils
sTake n (ConsS a l) = sTake (n - 1) l
sTake 1 (ConsSt a l) = a
sTake n (ConsSt a l) = sTake (n - 1) l

flatten :: SList a -> SList a
flatten Nils = Nils
flatten (ConsS a l) = ConsS a (flatten l)
flatten (ConsSt a l) = flatten (concatSList a l)

deepLength :: SList a -> Int
deepLength Nils = 0
deepLength (ConsS a l) = 1 + (deepLength l)
deepLength (ConsSt a l) = (deepLength a) + (deepLength l)

checkTake = sTake 5 (ConsS 2 (ConsS 5 (ConsSt (ConsS 9 Nils)(ConsS 8 (ConsS 11 (ConsS 15 Nils)))) ))
checkflatten = flatten (ConsS 2 (ConsS 5 (ConsSt (ConsS 9 Nils)(ConsS 8 (ConsS 11 (ConsS 15 Nils)))) ))
checkdeepLength = deepLength (ConsS 2 (ConsS 5 (ConsSt (ConsS 9 (ConsS 23 Nils))(ConsS 8 (ConsS 11 (ConsS 15 Nils)))) ))

{-
 3.(3p)
Definiti un tip de date BST a pentru a implementa un arbore binar de cautare.
De asemenea, definiti functii pentru a crea un arbore binar de cautare de la o
lista de elemente, cautarea unui element intr-un arbore binar de cautare si o 
functie care intoarce lista elementelor din parcurgerea in preordine a arborelui.

Hint: Este de preferat ca arborele binar de cautare sa fie balansat, lucru usor
de obtinut la creare daca lista de elemente este sortata
 -}

data BST a = Null | Cons1 (BST a) a (BST a) deriving Show

makeBST :: (Ord a) => [a] -> BST a
makeBST [] = Null
makeBST (a : l) = insertBST a (makeBST l)

insertBST a Null = Cons1 Null a Null
insertBST a (Cons1 a1 b a2) = if(a < b) then (Cons1 (insertBST a a1) b a2) else (Cons1 a1 b (insertBST a a2))

findElem :: (Ord a) => BST a -> a -> Maybe a
findElem Null n = Nothing
findElem (Cons1 a1 b a2) n = if(n == b) then Just n else if(n < b) then (findElem a1 n) else (findElem a2 n)

preorder :: BST a -> [a]
preorder Null = []
preorder (Cons1 a1 b a2) = preorder a1 ++ [b] ++ preorder a2

checkmakeBST = makeBST [5, 8, 2, 1, 4]
checkfindElem = findElem (makeBST [5, 8, 2, 1, 4]) 4
checkpreorder = preorder (makeBST [5, 8, 2, 1, 4])

{-
4.(2p)
Considerati tipul de date HuffTree care va reprezenta un arbore Huffman pentru
codificare. Implementati functia pentru construirea bottom-up a unui arbore Huffman.
Functia primeste o lista de perechi (Int, Char) 
-}

data HuffTree = Node Int HuffTree HuffTree | Leaf (Int, Char) deriving Show

makeHuffTree :: [(Int, Char)] -> HuffTree
makeHuffTree list = undefined


{-
 5. (2p) Bonus
Considerati tipul Tile definit pentru a reprezenta o imagine. Scopul exercitiului
este de a implementa functiile de manipulare a unui Tile folosind point-free
programming.
-}

type Tile = [String] 

cross = [".#..."
		,"#####"
		,".#..."
		,".#..."
		,".#..."]

{- Functiile pentru afisare. Ex:
 > putPic cross 
 .#...
 #####
 .#...
 .#...
 .#...
 -}

showPic [] = ""
showPic (line:lines) = line ++ "\n" ++ (showPic lines)

putPic = putStr . showPic

{-
flipH
_____

Flip horizontally.

> putPic (flipH cross)
.#...
.#...
.#...
#####
.#...
-}

flipH = reverse

{-
flipV
_____

Flip vertically.

> putPic (flipV cross)
...#.
#####
...#.
...#.
...#.
-}

flipV = map reverse

{-
rotate
______

Rotate 180 degrees.

> putPic (rotate cross)
...#.
...#.
...#.
#####
...#.
-}

rotate = reverse . map reverse

{-
above
_____

Places one pic on top of the other.

> putPic (above cross cross)
.#...
#####
.#...
.#...
.#...
.#...
#####
.#...
.#...
.#...
-}

above = (++)

{-
sideBySide
__________

Places one pic next to the other.

HINT! zipWith

> putPic (sideBySide cross cross)
.#....#...
##########
.#....#...
.#....#...
.#....#...
-}

sideBySide = zipWith (++)

{-
invert
______

Swaps the two characters (# -> . and . -> #).

> putPic (invert cross)
#.###
.....
#.###
#.###
#.###
-}
swap :: Char -> Char
swap '#' = '.'
swap '.' = '#'

invert = map (map swap)
