% 1. (1p)
% Concatenarea a doua liste.
% myConcat(+List1,+List2,-List)
myConcat([],List,List).
myConcat([Prim|Res], List, [Prim|Concat]):- myConcat(Res, List, Concat).


check1:-
	myConcat([],[],[]),
	myConcat([],[1,2],[1,2]), 
	myConcat([1,2,3],[4,5],[1,2,3,4,5]).

% 2. (1p)
% Obtine elementul de pe pozitia data dintr-o lista.
% getElem(+Index,+List,-Elem)
getElem(1, [Prim|Rest], Prim).
getElem(X, [Prim|Rest], Res) :- K is X - 1, getElem(K, Rest, Res).

check2:- 
	not(getElem(-1,[1,2],_)),
	getElem(1,[1,2],1),
	getElem(3,[1,2,3,4],3),
	not(getElem(5,[1,2,3,4],_)).

% 3. (1p)
% Obtine factorialul unui numar.
% myFact(+N,-Fact)
myFact(0,1).
myFact(N, Res) :- K is N - 1, myFact(K, R), Res is N * R.

check3:-
	myFact(1,1),
	myFact(4,24),
	myFact(7,5040).

% 4. (1.5p)
% Verifica daca lista data este simetrica.
% Exemple de liste simetrice: [1,2,3,2,1], [1,2,2,1],[],[1],etc.
% simetric(+List)
simetric(L) :- reverse(L,L).

check4 :-
	simetric([1,2,3,2,1]),
	simetric([1,2,2,1]),
	simetric([1]),
	simetric([]),
	not(simetric([1,2])).


% Se dau urmatoarele fapte care descriu arborele genealogic al unei familii:
parinte(ioana,radu).
parinte(ioana,miruna).
parinte(alin,radu).
parinte(alin,miruna).
parinte(ana,mihai).
parinte(ana,wendy).
parinte(radu,mihai).
parinte(radu,wendy).
parinte(mihai,dragos).
parinte(mihai,sorina).
parinte(mihai,ema).
parinte(elena,dragos).
parinte(elena,sorina).
parinte(elena,ema).

% 5. (2p)
% Scrieti urmatoarele reguli pentru a putea obtine mai multe informatii legate
% de relatiile de rudenie.

% soti(+Sot1,?Sot2)
% frate(+Frate1,?Frate2) 

% Observatii:
%  - daca unul din argumente este o variabila, toate valorile sale vor fi generate, 
% pe rand, la introducerea caracterului ';'
%  - valorile NU se vor repeta!
%  - un om nu poate fi propriul stramos, propriul frate sau sot

stramos(S, N):- parinte(S, P),
				   stramos(P, N).
stramos(P, F):- parinte(P, F).

soti(P1, P2):- parinte(P1, F) ,!, 
				 parinte(P2, F),
				 P1 \= P2.
frate(F1, F2):- parinte(P, F1) ,!, 
				 parinte(P, F2),
				 F1 \= F2.


	
check5:-
	stramos(ana,sorina),
	findall(X,stramos(ana,X),L1),
	permutation([wendy,ema,mihai,sorina,dragos],L1),!,
	findall(X,stramos(mihai,X),L2),
	permutation([ema,sorina,dragos],L2),!,
	findall(X,stramos(X,mihai),L3),
	permutation([ana,radu,ioana,alin],L3),!,
	soti(radu,ana),
	not(soti(dragos,ema)),
	findall(X,soti(mihai,X),L4),
	permutation([elena],L4),!,
	frate(mihai,wendy),
	not(frate(mihai,ana)),
	findall(X,frate(sorina,X),L5),
	permutation([dragos,ema],L5),!.

% 6. (1.5p)
% Scrieti o regula:
% rude_alianta(+Ruda1,?Ruda2)
% care verifica daca doua persoane sunt rude prin alianta.
% Atunci cand Ruda2 este o variabila neinstantiata, se genereaza
% pe rand toate rudele prin alianta ale lui Ruda1.
rude_alianta(Ruda1, Ruda2) :- parinte(Ruda1, Ruda2), soti(Ruda1, Ruda2), frati(Ruda1, Ruda2).
	
check6:-
	findall(X,rude_alianta(elena,X),L1),
	permutation([wendy,mihai,radu,ana,miruna,ioana,alin],L1),!,
	findall(X,rude_alianta(ana,X),L2),
	permutation([miruna,ioana,alin,radu],L2),!,
	findall(X,rude_alianta(radu,X),L3),
	permutation([ana],L3),!,
	findall(X,rude_alianta(alin,X),L4),
	permutation([ioana],L4),!.


	
% 7. (2p)
% Obtine o lista cu toate sublistele unei liste.
% Elementele din subliste apar in aceeasi ordice ca
% in lista initiala.
% getSublists(+List,-PartsList)
getSub([], []).
getSub([Prim|Rest], [Prim|Rest1]) :- getSub(Rest, Rest1).
getSub(Rest, [X|Rest1]) :- getSub(Rest, Rest1).
getSublists(L, P) :- findall(X, getSub(X, L), P).
	
check7:-
	getSublists([1,2],L1),
	permutation([[],[1],[2],[1,2]],L1),!,
	getSublists([1,2,3],L2),
	permutation([[],[1],[2],[3],[1,2],[1,3],[2,3],[1,2,3]],L2),!.

% Bonus 8. (2p)
% Sorteaza folosind mergesort.
% myMergesort(+List,-SortedList)
myMergesort([], []).
myMergesort([X], [X]).
	
checkBonus:-
	myMergesort([1],[1]),
	myMergesort([],[]),
	myMergesort([3,4,2,0,11,1],[0,1,2,3,4,11]),
	myMergesort([5,4,3,2,1],[1,2,3,4,5]).
	
checkAll:-
	check1,
	check2,
	check3,
	check4,
	check5,
	check6,
	check7,
	checkBonus.
